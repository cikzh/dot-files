if status is-interactive
    # Commands to run in interactive sessions can go here
    set -x GPG_TTY (tty)
    set -x SSH_AUTH_SOCK (gpgconf --list-dirs agent-ssh-socket)
    gpgconf --launch gpg-agent
end

fish_add_path ~/.local/bin

set -x USER_ID $(id -u)
set -x GROUP_ID $(id -g)

abbr -a gs git status
abbr -a d docker compose
