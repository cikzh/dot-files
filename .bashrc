#!/usr/bin/env bash

# Environment
export EDITOR=emacs
export CLICOLOR=1
export LSCOLORS=gxBxhxDxfxhxhxhxhxcxcx

# Path to the bash it configuration
export BASH_IT="$HOME/.bash_it"

# Lock and Load a custom theme file
# location /.bash_it/themes/
export BASH_IT_THEME='zork'

# Your place for hosting Git repos. I use this for private repos.
export GIT_HOSTING='git@tgrep.nl'

# Don't check mail when opening terminal.
unset MAILCHECK

# Change this to your console based IRC client of choice.
export IRC_CLIENT='irssi'

# Set this to the command you use for todo.txt-cli
export TODO="t"

# Set this to false to turn off version control status checking within the prompt for all themes
export SCM_CHECK=true

# Set vcprompt executable path for scm advance info in prompt (demula theme)
# https://github.com/xvzf/vcprompt
#export VCPROMPT_EXECUTABLE=~/.vcprompt/bin/vcprompt

export USER_ID=$(id -u)
export GROUP_ID=$(id -g)

# Load Bash It
source $BASH_IT/bash_it.sh

# Bash autocompletions
source /etc/profile.d/bash_completion.sh

if [ -f $HOME/.bash_aliases ]; then
  source $HOME/.bash_aliases
fi

export PATH="$HOME/.cargo/bin:$HOME/.local/bin:$PATH"

alias d="docker compose"
alias nuke='if [ "$(docker ps -q)" ]; then docker kill $(docker ps -q); fi'

# aliases
alias ll="ls -alh --color=auto"
alias gs="git status"
alias k="kubectl"
complete -o default -F __start_kubectl k

# If not running interactively, don't do anything
case $- in
  *i*) ;;
    *) return;;
esac

case ${TERM} in
 alacritty)
          PROMPT_COMMAND=${PROMPT_COMMAND:+$PROMPT_COMMAND; }'printf "\033]0;%s@%s:%s\007" "${USER}" "${HOSTNAME%%.*}" "${PWD/#$HOME/\~}"'
          ;;
esac
. "$HOME/.cargo/env"

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
