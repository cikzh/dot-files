;; Initialize package sources
(require 'package)
(setq package-archives '(("melpa" . "https://melpa.org/packages/")
                         ("org" . "https://orgmode.org/elpa/")
                         ("elpa" . "https://elpa.gnu.org/packages/")))

(package-initialize)
(unless package-archive-contents
  (package-refresh-contents))

;; Initialize use-package on non-linux platforms
(unless (package-installed-p 'use-package)
  (package-install 'use-package))

(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name
        "straight/repos/straight.el/bootstrap.el"
        (or (bound-and-true-p straight-base-dir)
            user-emacs-directory)))
      (bootstrap-version 7))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://radian-software.github.io/straight.el/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

(add-to-list 'load-path "~/.emacs.d/packages/")

(require 'use-package)
(setq use-package-always-ensure t)

(add-to-list 'load-path "~/.emacs.d/lisp/")

;; Determine platform
(setq cikzh/aquamacs (string-equal system-type "darwin"))
(setq cikzh/linux (string-equal system-type "gnu/linux"))
(setq cikzh/win32 (not (or cikzh/aquamacs cikzh/linux)))

(setq gc-cons-threshold 100000000)
(setq read-process-output-max (* 1024 1024)) ;; 1mb

(defun cikzh/startup ()
  (org-roam-dailies-goto-today)
  (message "Emacs loaded in %s with %d garbage collections."
           (format "%.2f seconds"
                   (float-time
                    (time-subtract after-init-time before-init-time)))
           gcs-done))

(add-hook 'emacs-startup-hook #'cikzh/startup)
(add-hook 'after-init-hook 'global-company-mode)

(setq inhibit-startup-message t)
(scroll-bar-mode -1)                         ;; Remove scroll bar
(tool-bar-mode -1)                           ;; Remove toolbar
(tooltip-mode -1)                            ;; Disable tooltips
(menu-bar-mode -1)                           ;; Disable menu bar
(global-hl-line-mode 1)                      ;; Highlight the current line
(column-number-mode)                         ;; Line numbers
(global-display-line-numbers-mode t)         ;; Line numbers
(setq-default truncate-lines t)              ;; Don't wrap
(setq-default indent-tabs-mode nil)          ;; Use spaces instead of tabs
(delete-selection-mode 1)                    ;; Marked text is being replaced
(global-unset-key [mouse-2])                 ;; no screwing with the middle mouse button
(setq scroll-step 3)                         ;; Smooth scroll
(save-place-mode 1)                          ;; Returns to the last point when revisiting a file
(global-auto-revert-mode 1)                  ;; Reverts the buffer, if no unsaved changes, when file is changed
(setq global-auto-revert-non-file-buffers t) ;; Same for dired

(when cikzh/linux
  (set-face-font 'default "DejaVu Sans Mono"))
(when cikzh/win32
  (set-face-font 'default t :font "Consolas"))

(setq-default line-spacing 3)

(use-package all-the-icons
  :config
  ;; Make sure the icon fonts are good to go
  (set-fontset-font t 'unicode (font-spec :family "all-the-icons") nil 'append)
  (set-fontset-font t 'unicode (font-spec :family "file-icons") nil 'append)
  (set-fontset-font t 'unicode (font-spec :family "Material Icons") nil 'append)
  (set-fontset-font t 'unicode (font-spec :family "github-octicons") nil 'append)
  (set-fontset-font t 'unicode (font-spec :family "FontAwesome") nil 'append)
  (set-fontset-font t 'unicode (font-spec :family "Weather Icons") nil 'append))

;; Don't use line numbers in these modes
(dolist (mode '(org-mode-hook
                term-mode-hook
                shell-mode-hook
                markdown-mode-hook
                compilation-mode-hook
                xref--xref-buffer-mode-hook
                eshell-mode-hook))
  (add-hook mode (lambda () (display-line-numbers-mode 0))))

;; Use line wrapping in these modes
(dolist (mode '(org-mode-hook
                compilation-mode-hook
                compilation-minor-mode-hook
                markdown-mode-hook))
  (add-hook mode (lambda () (visual-line-mode 1))))

;; Use line wrapping in these modes
(dolist (mode '(org-mode-hook
                markdown-mode-hook
                typst-ts-mode-hook
                compilation-mode-hook
                compilation--mindor-mode-hook))
  (add-hook mode (lambda () (visual-line-mode 1))))

(defadvice set-mark-command (after no-bloody-t-m-m activate)
  "Prevent consecutive marks activating bloody `transient-mark-mode'."
  (if transient-mark-mode (setq transient-mark-mode nil)))

;;(setq-default line-spacing 0)
 (use-package doom-themes
   :config
   ;; Global settings (defaults)
   (setq doom-themes-enable-bold t    ; if nil, bold is universally disabled
         doom-themes-enable-italic t) ; if nil, italics is universally disabled
   (load-theme 'monokai t)

   ;; Enable flashing mode-line on errors
   ;; (doom-themes-visual-bell-config)
   ;; Enable custom neotree theme (all-the-icons must be installed!)
   (doom-themes-neotree-config)
   ;; or for treemacs users
   (setq doom-themes-treemacs-theme "doom-atom") ; use "doom-colors" for less minimal icon theme
   (doom-themes-treemacs-config)
   ;; Corrects (and improves) org-mode's native fontification.
   (doom-themes-org-config))

(use-package lambda-line
  :straight (:type git :host github :repo "lambda-emacs/lambda-line") 
  :custom
  (lambda-line-position 'top) ;; Set position of status-line 
  (lambda-line-abbrev t) ;; abbreviate major modes
  (lambda-line-hspace "  ")  ;; add some cushion
  (lambda-line-prefix t) ;; use a prefix symbol
  (lambda-line-prefix-padding nil) ;; no extra space for prefix 
  (lambda-line-status-invert nil)  ;; no invert colors
  (lambda-line-gui-ro-symbol  " ⨂") ;; symbols
  (lambda-line-gui-mod-symbol " ⬤") 
  (lambda-line-gui-rw-symbol  " ◯") 
  (lambda-line-space-top +.40)  ;; padding on top and bottom of line
  (lambda-line-space-bottom -.40)
  (lambda-line-symbol-position 0.1) ;; adjust the vertical placement of symbol
  (lambda-line-vc-symbol "  ")
  (lambda-line-visual-bell nil)
  (lambda-line-git-diff-mode-line t)
  :config
  (customize-set-variable 'flymake-mode-line-counter-format '("" flymake-mode-line-error-counter flymake-mode-line-warning-counter flymake-mode-line-note-counter ""))
  (customize-set-variable 'flymake-mode-line-format '(" " flymake-mode-line-exception flymake-mode-line-counters))
  ;; activate lambda-line 
  (lambda-line-mode) 
  ;; set divider line in footer
  (when (eq lambda-line-position 'top)
    (setq-default mode-line-format (list "%_"))
    (setq mode-line-format (list "%_"))))

(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode)
  :config (set-face-attribute 'rainbow-delimiters-unmatched-face nil :foreground "red" :background "white"))

;; Two buffers side-by-side
(split-window-horizontally)

(setq split-width-threshold (- (window-width) 10))
(setq split-height-threshold nil)

(defun count-visible-buffers (&optional frame)
  "Count how many buffers are currently being shown. Defaults to selected frame."
  (length (mapcar #'window-buffer (window-list frame))))

(defun do-not-split-more-than-two-windows (window &optional horizontal)
  (if (and horizontal (> (count-visible-buffers) 1))
      nil
    t))

(advice-add 'window-splittable-p :before-while #'do-not-split-more-than-two-windows)

;; Treemacs
(use-package treemacs
  :config
  (progn
    (setq treemacs-collapse-dirs                    (if treemacs-python-executable 3 0)
          treemacs-deferred-git-apply-delay         0.5
          treemacs-directory-name-transformer       #'identity
          treemacs-display-in-side-window           t
          treemacs-eldoc-display                    t
          treemacs-file-event-delay                 5000
          treemacs-file-extension-regex             treemacs-last-period-regex-value
          treemacs-file-follow-delay                0.1
          treemacs-file-name-transformer            #'identity
          treemacs-follow-after-init                nil
          treemacs-git-command-pipe                 ""
          treemacs-goto-tag-strategy                'refetch-index
          treemacs-indentation                      1
          treemacs-indentation-string               " "
          treemacs-is-never-other-window            t
          treemacs-max-git-entries                  5000
          treemacs-missing-project-action           'ask
          treemacs-move-forward-on-expand           nil
          treemacs-no-png-images                    nil
          treemacs-no-delete-other-windows          nil
          treemacs-project-follow-cleanup           nil
          treemacs-persist-file                     (expand-file-name ".cache/treemacs-persist" user-emacs-directory)
          treemacs-position                         'left
          treemacs-recenter-distance                0.3
          treemacs-recenter-after-file-follow       nil
          treemacs-recenter-after-tag-follow        nil
          treemacs-litter-directories               '("/node_modules" "/.cache")
          treemacs-recenter-after-project-jump      'always
          treemacs-recenter-after-project-expand    'on-distance
          treemacs-select-when-already-in-treemacs  'move-back
          treemacs-show-cursor                      nil
          treemacs-show-hidden-files                t
          treemacs-silent-filewatch                 nil
          treemacs-silent-refresh                   nil
          treemacs-sorting                          'alphabetic-asc
          treemacs-space-between-root-nodes         t
          treemacs-tag-follow-cleanup               t
          treemacs-tag-follow-delay                 1.5
          treemacs-user-mode-line-format            nil
          treemacs-user-header-line-format          nil
          treemacs-width                            30
          treemacs-workspace-switch-cleanup         nil)

    ;; The default width and height of the icons is 22 pixels. If you are
    ;; using a Hi-DPI display, uncomment this to double the icon size.
    ;;(treemacs-resize-icons 44)

    (treemacs-filewatch-mode t)
    (treemacs-fringe-indicator-mode t)
    (treemacs-define-RET-action 'file-node-closed #'treemacs-visit-node-in-most-recently-used-window)
    (treemacs-define-RET-action 'file-node-open #'treemacs-visit-node-in-most-recently-used-window)
    (pcase (cons (not (null (executable-find "git")))
                 (not (null treemacs-python-executable)))
      (`(t . t)
       (treemacs-git-mode 'deferred))
      (`(t . _)
       (treemacs-git-mode 'simple))))
  :bind
  (:map global-map
        ("M-0"       . treemacs-select-window)
        ("C-x t 1"   . treemacs-delete-other-windows)
        ("C-x t t"   . treemacs)
        ("C-x t B"   . treemacs-bookmark)
        ("C-x t M-t" . treemacs-find-tag))
  :custom-face
  (treemacs-directory-face ((t (:background "#272822" :foreground "gray" :weight normal)))))

(use-package treemacs-projectile
  :after treemacs projectile)

(use-package treemacs-icons-dired
  :after treemacs dired
  :config (treemacs-icons-dired-mode))

(use-package treemacs-magit
  :after treemacs magit)

(use-package treemacs-persp ;;treemacs-persective if you use perspective.el vs. persp-mode
  :after treemacs persp-mode ;;or perspective vs. persp-mode
  :config (treemacs-set-scope-type 'Perspectives))

(require 'xref)
(set-face-attribute 'xref-match nil :background "#2F2F4F4F4F4F" :foreground "white" :inherit 'weight)

(bind-keys
 :map global-map
 ("C-q" . kill-ring-save)
 ("C-w" . delete-region)
 ("C-S-w" . delete-rectangle)
 ("C-r" . kill-region)
 ("C-f" . yank)
 ("C-v" . yank-pop)
 ("C-d" . quick-copy-line)
 ("C-S-d" . duplicate-current-line)
 ("M-w" . other-window)
 ("M-W" . ace-swap-window)
 ("M-k" . kill-current-buffer)
 ("\C-z" . undo)
 ("C-s" . save-buffer)
 ("C-x C-r" . revert-buffer)
 ("C-x C-d" . dired)
 ("<C-tab>" . indent-region)
 ("C-t" . kill-whole-line)
 ("C-\\" . join-line)
 ("C-a" . back-to-indentation)
 ("M-F" . find-file-other-window)
 ("M-B" . switch-to-buffer-other-window)
 ("S-C-F" . projectile-find-file)
 ("M-p" . quick-calc)
 ("M-o" . query-replace)
 ("M-O" . replace-string)
 ("M-;" . comment-or-uncomment-region)

 ("<f2>" . counsel-projectile-rg)
 ("<f3>" . sql-postgres)
 ("<f4>" . string-inflection-all-cycle)
 ("<f6>" . org-timer-set-timer)
 ("S-<f4>" . string-inflection-lower-camelcase)
 ("C-<f4>" . string-inflection-underscore)
 ("<f7>" . counsel-mark-ring)
 ("C-c h" . eldoc)
 ("M-[" . start-kbd-macro)
 ("M-]" . end-kbd-macro)
 ("M-'" . call-last-kbd-macro)

 ("M-n" . next-error)
 ("M-N" . previous-error)

 ("<S-M-left>" . smerge-keep-mine)
 ("<S-M-right>" . smerge-keep-other)
 ("<S-M-up>" . smerge-prev)
 ("<S-M-down>" . smerge-next)
 ("C-c n b" . cikzh/org-roam-capture-inbox)
 ("M-i" . cikzh/org-roam-goto-inbox))

(use-package ivy
  :diminish
  :bind(("M-s" . swiper)
        :map ivy-minibuffer-map
        ("TAB" . ivy-alt-done))
  :config
  (ivy-mode 1))

(use-package swiper
  :custom-face (swiper-background-match-face-2 ((t (:inherit swiper-match-face-2 :background "dark goldenrod")))))

(use-package ivy-rich
  :after ivy
  :init (ivy-rich-mode 1))

(use-package ivy-hydra)

(setq eldoc-echo-area-use-multiline-p nil)
(setq eldoc-echo-area-display-truncation-message t)

(use-package magit
  :defer 3)

(use-package smex
  :bind ("<S-M-left>" . smerge-keep-mine)
  ("<S-M-right>" . smerge-keep-other)
  ("<S-M-up>" . smerge-prev)
  ("<S-M-down>" . smerge-next))

(use-package counsel
  :bind (("M-x" . counsel-M-x)
         ("M-f" . counsel-find-file)
         ("M-b" . counsel-switch-buffer)
         )
  :custom
  (counsel-linux-app-format-function #'counsel-linux-app-format-function-name-only)
  :config
  ;;(setcdr (assoc 'counsel-M-x ivy-initial-inputs-alist) "") ;; Don't start searches with ^
  (setq ivy-initial-inputs-alist nil)
  (counsel-mode t))

(use-package which-key
  :defer t
  :diminish which-key-mode
  :config
  (which-key-mode)
  (setq which-key-idle-delay 1))

(defun cikzh/org-mode-setup ()
  (org-indent-mode)
  (variable-pitch-mode 1)
  (auto-fill-mode 0)
  ;; Download the sound at https://freesound.org/people/.Andre_Onate/sounds/484665/
  (setq org-clock-sound "~/Downloads/unfa_ping.wav")
  (require 'ox-md))

(defun cikzh/org-font-setup ()
  ;; Replace list hyphen with dot
  (font-lock-add-keywords 'org-mode
                          '(("^ *\\([-]\\) "
                             (0 (prog1 () (compose-region (match-beginning 1) (match-end 1) "•"))))))

  ;; Ensure that anything that should be fixed-pitch in Org files appears that way
  (set-face-attribute 'org-block nil :foreground nil :inherit 'fixed-pitch)
  (set-face-attribute 'org-code nil   :inherit '(shadow fixed-pitch))
  (set-face-attribute 'org-table nil   :inherit '(shadow fixed-pitch))
  (set-face-attribute 'org-verbatim nil :inherit '(shadow fixed-pitch))
  (set-face-attribute 'org-special-keyword nil :inherit '(font-lock-comment-face fixed-pitch))
  (set-face-attribute 'org-meta-line nil :inherit '(font-lock-comment-face fixed-pitch))
  (set-face-attribute 'org-checkbox nil :inherit 'fixed-pitch))

(use-package org
  :ensure t
  :pin org
  :hook (org-mode . cikzh/org-mode-setup)
  :bind (:map org-mode-map
              ("M-," . org-mark-ring-goto))
  :config
  (setq org-ellipsis " ▾")
  (setq org-return-follows-link  t)
  (cikzh/org-font-setup))

(use-package org-bullets
  :after org
  :hook (org-mode . org-bullets-mode))

(with-eval-after-load 'org
  ;; This is needed as of Org 9.2
  (require 'org-tempo)
  (add-to-list 'org-structure-template-alist '("el" . "src emacs-lisp"))
  (add-to-list 'org-structure-template-alist '("js" . "src js"))
  (add-to-list 'org-structure-template-alist '("ts" . "src typescript"))
  (add-to-list 'org-structure-template-alist '("rust" . "src rust"))
  (add-to-list 'org-structure-template-alist '("sh" . "src shell")))

(use-package org-roam
  :init
  (setq org-roam-v2-ack t)
  :custom
  (org-roam-completion-everywhere t)
  (org-roam-dailies-capture-templates
   '(("d" "default" entry "* %?"
      :if-new (file+head "%<%Y-%m-%d>.org" "#+title: %<%Y-%m-%d>\n"))))
  (org-roam-directory (file-truename "~/Sync/org-roam/"))
  :bind (("C-c n l" . org-roam-buffer-toggle)
         ("C-c n f" . org-roam-node-find)
         ("C-c n g" . org-roam-graph)
         ("C-c n i" . org-roam-node-insert)
         ("<f8>" . org-roam-dailies-capture-today)
         :map org-mode-map
         ("C-M-i" . completion-at-point)
         :map org-roam-dailies-map
         ("Y" . org-roam-dailies-capture-yesterday)
         ("T" . org-roam-dailies-capture-tomorrow))
  :bind-keymap
  ("C-c n d" . org-roam-dailies-map)
  :config
  (org-roam-setup)
  (require 'org-roam-dailies)
  ;; If using org-roam-protocol
  (require 'org-roam-protocol)
  (org-roam-db-autosync-mode)

  (add-to-list 'org-after-todo-state-change-hook
               (lambda ()
                 (when (equal org-state "DONE")
                   (cikzh/org-roam-copy-todo-to-today)))))

(defun cikzh/org-roam-capture-inbox ()
  (interactive)
  (org-roam-capture- :node (org-roam-node-create)
                     :templates '(("i" "inbox" plain "* %?"
                                   :if-new (file+head "Inbox.org" "#+title: Inbox\n")))))

(defun cikzh/org-roam-goto-inbox ()
  (interactive)
  (org-roam-node-find nil "Inbox"))

;; Automatically tangle our Emacs.org config file when we save it
(defun cikzh/org-babel-tangle-config ()
  (when (string-equal (buffer-file-name)
                      (expand-file-name "~/dot-files/Emacs.org"))
    ;; Dynamic scoping to the rescue
    (let ((org-confirm-babel-evaluate nil))
      (org-babel-tangle))))

(add-hook 'org-mode-hook (lambda () (add-hook 'after-save-hook #'cikzh/org-babel-tangle-config)))

(setq make-backup-file-name-function (quote ignore))
(setq make-backup-files nil)
(setq auto-save-default nil)
(setq auto-save-interval 0)
(setq auto-save-list-file-prefix nil)
(setq auto-save-timeout 0)
(setq create-lockfiles nil)
(setq delete-auto-save-files nil)
(setq delete-old-versions (quote other))
(setq vc-make-backup-files nil)

;; Copies current line and moves point to beginning of next line
(defun quick-copy-line ()
  "Copy the whole line that point is on and move to the beginning of the next line.
    Consecutive calls to this command append each line to the
    kill-ring."
  (interactive)
  (let ((beg (line-beginning-position 1))
        (end (line-beginning-position 2)))
    (if (eq last-command 'quick-copy-line)
        (kill-append (buffer-substring beg end) (< end beg))
      (kill-new (buffer-substring beg end))))
  (beginning-of-line 2))

(setq require-final-newline t)

;; Duplicates current line downward
(defun duplicate-current-line (&optional n)
  "duplicate current line, make more than 1 copy given a numeric argument"
  (interactive "p")
  (save-excursion
    (let ((nb (or n 1))
          (current-line (thing-at-point 'line)))
      ;; when on last line, insert a newline first
      (when (or (= 1 (forward-line 1)) (eq (point) (point-max)))
        (insert "\n"))

      ;; now insert as many time as requested
      (while (> n 0)
        (insert current-line)
        (setq n (1- n))))))

(use-package move-text
  :init (move-text-default-bindings))

(use-package rg
  :init (rg-enable-default-bindings))

(use-package string-inflection)

(setq browse-url-generic-program "firefox")

(require 'treesit)
(setq treesit-language-source-alist
      '((bash "https://github.com/tree-sitter/tree-sitter-bash")
        (css "https://github.com/tree-sitter/tree-sitter-css")
        (elisp "https://github.com/Wilfred/tree-sitter-elisp")
        (go "https://github.com/tree-sitter/tree-sitter-go")
        (html "https://github.com/tree-sitter/tree-sitter-html")
        (json "https://github.com/tree-sitter/tree-sitter-json")
        (make "https://github.com/alemuller/tree-sitter-make")
        (markdown "https://github.com/ikatyang/tree-sitter-markdown")
        (php "https://github.com/tree-sitter/tree-sitter-php")
        (python "https://github.com/tree-sitter/tree-sitter-python")
        (svelte "https://github.com/Himujjal/tree-sitter-svelte/")
        (rust "https://github.com/tree-sitter/tree-sitter-rust")
        (toml "https://github.com/tree-sitter/tree-sitter-toml")
        (typescript "https://github.com/tree-sitter/tree-sitter-typescript" "master" "typescript/src")
        (tsx "https://github.com/tree-sitter/tree-sitter-typescript" "master" "tsx/src")
        (jsx "https://github.com/tree-sitter/tree-sitter-javascript" "master" "jsx/src")
        (typst "https://github.com/uben0/tree-sitter-typst")
        (yaml "https://github.com/ikatyang/tree-sitter-yaml")))
(push '(css-mode . css-ts-mode) major-mode-remap-alist)
(push '(js-json-mode . json-ts-mode) major-mode-remap-alist)
(push '(typescript-mode . typescript-ts-mode) major-mode-remap-alist)
(push '(c-mode . c-ts-mode) major-mode-remap-alist)
(push '(c++-mode . c++-ts-mode) major-mode-remap-alist)

(require 'eglot)
(setq eglot-extend-to-xref t)
(setq eglot-events-buffer-size 0)

;; No cluttering inlay hints please (well... no inlay hints at all)
(setq eglot-ignored-server-capabilities '(:inlayHintProvider))
(add-to-list 'eglot-server-programs
             '(tsx-ts-mode . ("typescript-language-server" "--stdio")))
(add-to-list 'eglot-server-programs
             '(typescript-ts-mode . ("typescript-language-server" "--stdio")))
(add-to-list 'eglot-server-programs
             '(javascript-ts-mode . ("typescript-language-server" "--stdio")))
(add-to-list 'eglot-server-programs
             '(php-mode . ("intelephense" "--stdio")))
(add-to-list 'eglot-server-programs
             '(web-mode . ("typescript-language-server" "--stdio")))
(add-to-list 'eglot-server-programs '((typst-ts-mode typst-mode) . ("typst-lsp")))
(add-to-list 'eglot-server-programs
             '(svelte-mode . ("svelteserver" "--stdio")))


(setq eglot-connect-hook nil)

(bind-keys
 :map eglot-mode-map
 ("C-c f" . eglot-format)
 ("C-c r" . eglot-rename)
 ("M-e" . eglot-code-actions)
 ("C-c e" . eglot-code-action-quickfix)
 ("C-c i" . eglot-code-action-organize-imports)
 ("C-c h" . eldoc)
 ("M-/" . xref-find-references))

(use-package company
  :bind (:map company-active-map
              ("<tab>" . company-complete-selection))
  :custom
  (company-minimum-prefix-length 3)
  (company-idle-delay 0.0))

(defun cikzh/compile-ts ()
  "Compiles the current project with the TypeScript compiler"
  (interactive)
  (compile "tsc --noEmit --allowJs --pretty false"))

(use-package typescript-ts-mode
  :hook ((typescript-ts-mode . eglot-ensure)
         (typescript-ts-mode . company-mode))
  :bind (("M-n" . flymake-goto-next-error)
         ("M-N" . flymake-goto-prev-error))
  :mode (("\\.ts$" . typescript-ts-mode)
         ("\\.tsx$" . tsx-ts-mode)
         ("\\.jsx$" . tsx-ts-mode))

  :config
  (setq typescript-indent-level 2))

(use-package svelte-mode
  :hook ((svelte-mode . eglot-ensure)
         (svelte-mode . company-mode))
  :mode ("\\.svelte$" . svelte-mode)
  :config
  (setq svelte-tag-relative-indent nil)
  (setq svelte-display-submode-name t))

;; We also need rust-mode for compilation-mode stuff
(use-package rust-mode
  :hook (
         (rust-mode . eglot-ensure)
         (rust-mode . company-mode))
  :init
  (setq rust-mode-treesitter-derive t)
  :bind  ("M-m" . rust-check)
  :config
  (eglot-inlay-hints-mode -1)
  (indent-tabs-mode nil))

(use-package cargo
  :hook (rust-mode . cargo-minor-mode)
  :bind (("M-M" . cargo-process-current-file-tests)))

(use-package ron-mode)

(use-package php-mode
  :hook (php-mode . eglot-ensure)
  :mode ("\\.php\\'" . php-mode))

(use-package phpunit
  :ensure t)

(provide 'lang-php)

(use-package yaml-ts-mode
  :config
  (setq yaml-indent-offset 4))

(use-package dockerfile-mode)

(use-package wgsl-mode)

;; load typst-mode
(use-package typst-ts-mode
  :straight (:type git :host sourcehut :repo "meow_king/typst-ts-mode" :files (:defaults "*.el"))
  :custom
  ;; don't add "--open" if you'd like `watch` to be an error detector
  (typst-ts-mode-watch-options "--open")
  (typst-ts-mode-indent-offset 2))

(load-file "~/dot-files/game-brain-mode.el")
(require 'game-brain-mode)
(add-to-list 'auto-mode-alist '("\\.gbr$" . game-brain-mode))

(use-package fish-mode)

(use-package projectile
  :diminish projectile-mode
  :config (projectile-mode)
  :custom ((projectile-completion-system 'ivy))
  :bind-keymap
  ("C-c p" . projectile-command-map)
  :bind (("M-K" . projectile-kill-buffers))
  :init
  (when (file-directory-p "~/")
    (setq projectile-project-search-path '("~/")))
  (setq projectile-switch-project-action #'projectile-dired))

(use-package counsel-projectile
  :config (counsel-projectile-mode))

(use-package projectile-ripgrep)

(use-package yasnippet
  :hook ((text-mode . yas-minor-mode-on)
         (prog-mode . yas-minor-mode-on)
         (conf-mode . yas-minor-mode-on)
         (snippet-mode . yas-minor-mode-on))
  :init
  (setq yas-snippet-dir "~/dot-files/.emacs.d/snippets")
  (yas-global-mode 1))

;; Helper function used in Yasnippets. Should be loaded before yansippet
(defun capitalizeFirst (s)
  (if (> (length s) 0)
      (concat (upcase (substring s 0 1)) (substring s 1))
    nil))

(use-package restclient
  :mode ("\\.http\\'" . restclient-mode))

(use-package helpful
  :commands (helpful-callable helpful-variable helpful-command helpful-key)
  :custom
  (counsel-describe-function-function #'helpful-callable)
  (counsel-describe-variable-function #'helpful-variable)
  :bind
  ([remap describe-function] . counsel-describe-function)
  ([remap describe-command] . helpful-command)
  ([remap describe-variable] . counsel-describe-variable)
  ([remap describe-key] . helpful-key))
