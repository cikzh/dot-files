(setq init-file-dir "~/.emacs.d")
(load-file "~/dot-files/init.el")

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(eglot-ignored-server-capabilities '(:inlayHintProvider))
 '(newsticker-url-list
   '(("typst-ts-mode" "https://git.sr.ht/~meow_king/typst-ts-mode/log/main/rss.xml" nil nil nil))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
