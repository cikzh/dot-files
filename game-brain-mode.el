;; (define-generic-mode 
;;     'game-brain-mode                                     ;; name of the mode to create
;;   '("#")                                                 ;; comments
;;   '("max" "min" "floor" "avg" "sum" "includes")          ;; keywords
;;   '(("^[a-zA-Z0-9_]+" . 'font-lock-type-face)            ;; matches snake_case var assignments at the beginning of a line
;;     ("[a-zA-Z0-9_]+\\[" . 'font-lock-variable-name-face) ;; matches snake_case var assignments anywhere
;;     ("[0-9]+d[0-9]+" . 'font-lock-variable-name-face)
;;     ("false" . 'font-lock-constant-face)
;;     ("true" . 'font-lock-constant-face))
;;   '("\\.gbr$")                                           ;; files for which to activate this mode 
;;   nil                                                    ;; other functions to call
;;   "A mode for game-brain files")                         ;; doc string for this mode

(defvar game-brain-constants
  '("true"
    "false"))

(defvar game-brain-keywords
  '("false" "true"))

(defvar game-brain-tab-width nil "Width of a tab for GAME-BRAIN mode")

(defvar game-brain-font-lock-defaults
  `((
     ;; stuff between double quotes
     ("\"\\.\\*\\?" . font-lock-string-face)
     ("[0-9]+d[0-9]+" . font-lock-type-face)
     ("[a-zA-Z0-9_]+\\[" . font-lock-variable-name-face)
     ("^[a-zA-Z0-9_]+" . font-lock-variable-name-face)
     ("[0-9]" . font-lock-constant-face)
     ( ,(regexp-opt game-brain-keywords 'words) . font-lock-builtin-face)
     ( ,(regexp-opt game-brain-constants 'words) . font-lock-constant-face)
     )))

(define-derived-mode game-brain-mode prog-mode "Game Brain"
  "game-brain mode is a major mode for editing .gbr files"
  ;; you again used quote when you had '((game-brain-hilite))
  ;; I just updated the variable to have the proper nesting (as noted above)
  ;; and use the value directly here
  (setq font-lock-defaults game-brain-font-lock-defaults)
  
  ;; when there's an override, use it
  ;; otherwise it gets the default value
  (when game-brain-tab-width
    (setq tab-width game-brain-tab-width))
  
  ;; for comments
  ;; they're made buffer local when you set them
  (setq comment-start "#")
  (setq comment-end "")
  
  (modify-syntax-entry ?# "< b" game-brain-mode-syntax-table)
  (modify-syntax-entry ?\n "> b" game-brain-mode-syntax-table)
  
  ;; Note that there's no need to manually call `game-brain-mode-hook'; `define-derived-mode'
  ;; will define `game-brain-mode' to call it properly right before it exits
  )

(provide 'game-brain-mode)
